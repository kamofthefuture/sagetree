Hackathon project 7 day fun run.

- Friday: get shell of UI done (3-5 hours)
- Saturday: write basic backend to fetch genetic info (3-5 hours)
- Sunday: connect UI to backend, deploy app in docker container (5 hours)
- Monday: Test flow of app (2 hours)
- Tuesday: Polish app (Loaders, animations, etc.)
- Wednesday: Record video and edit (1 hour)
- Thursday: Prepare submission

## Idea/ Outer Workings

- Genomelink gives me data about user X
- User X has a "curated healthfeed" based on which datapoints they choose to expose to my app
- Each dataset from Genomelink become a "curated" content space that can be altered based on a users data
- Each curate page links to different affiliated products 

## Engineering/ Inner Workings

### Frontend
 - React 
  - Pintrest Gestalt UI lib
 - Apollo Client

### Backend
 - Apollo Server
 - Docker
 - MYSQL DB

### Diagram
 ![sagetree diagram](//gitlab.com/kamofthefuture/sagetree/raw/master/diagrams/sagetree.png)


