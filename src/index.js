import React from 'react'
import ReactDOM from 'react-dom'
import { HttpLink, InMemoryCache, ApolloClient } from 'apollo-client-preset'
import { WebSocketLink } from 'apollo-link-ws'
import { ApolloLink, split } from 'apollo-link'
import { getMainDefinition } from 'apollo-utilities'
import { AUTH_TOKEN, GENOME_TOKEN } from './constant'
import RootContainer from './components/RootContainer'
import { ApolloProvider } from 'react-apollo'
import './index.css'
import 'gestalt/dist/gestalt.css'

const { NODE_ENV } = process.env
const PROD_API='//sagetreeapi.apps.kam.services'

const uri = NODE_ENV === 'production' ? `http:${PROD_API}` : 'http://localhost:4000'
const wsURI = NODE_ENV === 'production' ? `ws:${PROD_API}` : 'ws://localhost:4000'
console.log(process.env)
const httpLink = new HttpLink({ uri })

const middlewareLink = new ApolloLink((operation, forward) => {
  // get the authentication token from local storage if it exists
  const tokenValue = localStorage.getItem(AUTH_TOKEN)
  const oauthToken = localStorage.getItem(GENOME_TOKEN)
  // return the headers to the context so httpLink can read them
  operation.setContext({
    headers: {
      Authorization: tokenValue ? `Bearer ${tokenValue}` : '',
      OauthToken: oauthToken
        ? `Bearer ${localStorage.getItem(GENOME_TOKEN)}`
        : '',
    },
  })
  return forward(operation)
})

// authenticated httplink
const httpLinkAuth = middlewareLink.concat(httpLink)

const wsLink = new WebSocketLink({
  uri: wsURI,
  options: {
    reconnect: true,
    connectionParams: {
      Authorization: `Bearer ${localStorage.getItem(AUTH_TOKEN)}`,
      OauthToken: `Bearer ${localStorage.getItem(GENOME_TOKEN)}`,
    },
  },
})

const link = split(
  // split based on operation type
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === 'OperationDefinition' && operation === 'subscription'
  },
  wsLink,
  httpLinkAuth,
)

// apollo client setup
const client = new ApolloClient({
  link: ApolloLink.from([link]),
  cache: new InMemoryCache(),
  connectToDevTools: true,
})

const token = localStorage.getItem(AUTH_TOKEN)
const oauthToken = localStorage.getItem(GENOME_TOKEN)

ReactDOM.render(
  <ApolloProvider client={client}>
    <RootContainer token={token} oauthToken={oauthToken} />
  </ApolloProvider>,
  document.getElementById('root'),
)
