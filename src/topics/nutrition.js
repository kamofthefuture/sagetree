export default [
  {
    key: 'iron',
    color: '#2b3938',
    height: 316,
    about: 'Consequat occaecat consectetur voluptate cupidatat. Ullamco pariatur cillum sint fugiat aliquip proident ad. Consectetur pariatur deserunt esse mollit ipsum magna ut ea do. Minim non cupidatat voluptate commodo commodo anim. Eu enim fugiat ad cillum. Fugiat sint in qui laborum ipsum ullamco deserunt deserunt enim non nisi exercitation adipisicing. Eu velit irure est esse sint nostrud quis.',
  },
  {
    key: 'phosphorus',
    color: '#8e7439',
    height: 500,
    about: 'Voluptate Lorem irure reprehenderit labore ipsum sit deserunt nostrud esse sit occaecat ea. Sint laboris laborum aute ea. Adipisicing ea amet consectetur ad. Ea duis eiusmod magna elit qui irure est voluptate nisi exercitation cillum incididunt dolore. Esse nulla ullamco minim Lorem magna qui ut commodo consequat. Anim in labore adipisicing nisi deserunt mollit nostrud eiusmod Lorem. Esse mollit aliquip exercitation eiusmod tempor Lorem occaecat.',
  },
  {
    key: 'magnesium',
    color: '#698157',
    height: 411,
    about: 'Sit officia velit velit aliqua Lorem sint voluptate reprehenderit quis deserunt. Laboris id est exercitation cupidatat voluptate. Magna minim duis occaecat culpa cupidatat id laborum consequat proident ut magna eu quis proident. Nulla consequat sunt consectetur dolore. Nisi enim minim occaecat aute ullamco ullamco ipsum et et nisi ad reprehenderit. Velit proident exercitation id proident ad exercitation. Ex irure ipsum nulla anim amet irure.',
  },
  {
    key: 'calcium',
    color: '#4e5d50',
    height: 632,
    about: 'Est nisi minim exercitation cillum aliqua incididunt sit ullamco eu. Cillum consequat Lorem do ad. Aute magna nisi anim laborum officia.',
  },
  {
    key: 'folate',
    color: '#6d6368',
    height: 710,
    about: 'Officia mollit magna labore culpa qui aute excepteur voluptate adipisicing aliquip. Aliqua minim cillum sit duis deserunt laboris. Ea reprehenderit do nisi nostrud id fugiat laborum minim do non cillum duis magna dolore.',
  },
  {
    key: 'vitamin-e',
    color: '#eee',
    height: 630,
    about: 'Proident irure velit in consectetur cupidatat eiusmod officia eu cillum. Enim ex laboris consequat cupidatat minim laborum ex cupidatat officia. Magna ipsum voluptate aliqua labore eu ut pariatur dolore nostrud. Quis non et voluptate labore ex labore veniam enim ut.',
  },
  {
    key: 'vitamin-d',
    color: '#eee',
    height: 620,
    about: 'Laborum deserunt dolor proident est dolor esse. Aliqua laboris amet mollit minim tempor deserunt aliqua incididunt do tempor adipisicing. In esse ipsum pariatur irure et laborum occaecat voluptate. Nostrud labore dolor non minim ea commodo irure nostrud consequat. Fugiat eu tempor cupidatat tempor commodo magna quis proident veniam aliquip.',
  },
  {
    key: 'vitamin-b12',
    color: '#eee',
    height: 550,
    about: 'Consequat irure exercitation id adipisicing consequat cupidatat do nisi cillum consequat et et. Pariatur dolore non culpa aliqua et id quis dolore Lorem in quis amet. Ex sunt id est culpa labore duis et sit deserunt exercitation. Dolor in et velit velit velit minim do mollit. Nisi eu est adipisicing dolor laborum minim tempor enim elit qui nostrud officia officia laboris.',
  },
  {
    key: 'vitamin-a',
    color: '#eee',
    height: 490,
    about: 'Consequat voluptate non non mollit. Ex est officia in quis eiusmod non in nisi. Id labore laboris in deserunt sint occaecat in ad non. Qui nulla excepteur qui amet. Et voluptate ipsum eiusmod magna qui mollit ullamco id ad minim consequat consectetur sit ex. Sunt sit adipisicing commodo aliqua laboris laboris.',
  },
]