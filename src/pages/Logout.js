import React from 'react'
import { withRouter } from 'react-router-dom'
import { Box, Text } from 'gestalt'

const LogoutPage = () => (
  <Box>
    <Text>You are currently logged out. To see the drafts, please login.</Text>
  </Box>
)

export default withRouter(LogoutPage)
