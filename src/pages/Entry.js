import React from 'react'
import { Button } from 'gestalt'
import { withState } from 'recompact'
import Wizard from '../components/Wizard'
import { StepOne, StepTwo } from '../components/GettingStarted'
import Link from '../components/Link'

const pages = [
  { name: 'StepOne', step: 'who', component: StepOne },
  { name: 'StepTwo', step: 'mart', component: StepTwo },
]

const Entry = (props) => (
  <div>
    <Wizard
      {...props}
      onSubmit={(val) => alert(JSON.stringify(Array.from(props.list), null, 2))}
      onCancel={() => console.log('object')}
      Cancel={
        <Link to="/">
          <Button text="cancel" inline />
        </Link>
      }
      Next={
        <Button text="cancel" inline />
      }
      isForm={false}
      pages={pages}
    />
  </div>
)

export default withState('list', 'updateList', new Set())(Entry)
