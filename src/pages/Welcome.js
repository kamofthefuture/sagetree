import React from 'react'
import styled from 'styled-components'
import { Text, Box, Button, Mask } from 'gestalt'
import Link from '../components/Link'
import Cascade from '../components/Cascade'
import Page from '../components/Page'
import logo from '../assets/sage.png'

const Wrapper = styled(Cascade).attrs({ interval: 300 })`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 80vh;
  overflow: hidden;
`

const Welcome = () => (
  <Page>
    <Wrapper>
      <Box padding={3}>
        <Mask shape="rounded">
          <div>
            <img
              alt="sage"
              src={logo}
              style={{ maxWidth: '70px' }}
            />
          </div>
        </Mask>
      </Box>

      <Box padding={3}>
        <div>
          <Text color="midnight" align="center" bold size="xl">Welcome</Text>
        </div>
      </Box>

      <div>
        <Box maxWidth={350} padding={0}>
          <Text align="center" size="lg" color="midnight" overflow="breakWord">
            <Text inline color="olive">Sagetree</Text> is a curated health and wellness platform that utilizes genetics data to help you make informed preventative decisions.
      </Text>
          <Box padding={6}>
            <Link to="/entry">
              <Button text="Get Started" />
            </Link>
          </Box>
        </Box>
      </div>

      <div>
        <Box marginTop={6}>
          <Text
            align="center"
            size="xs"
            color="pine"
            overflow="breakWord"
          >
            All ideas provided are natural and organic.
        </Text>
        </Box>
      </div>
      {/* <Loader withText={false} className={css`margin-top:100rem;`}/> */}
    </Wrapper>
  </Page>
)

/**
 * 
 * <Text>Decision tree modal/ overlay</Text>
 * <Text>If login with genomelink show checklist</Text>
 * <Text>If no login forward to home</Text>
 * 
 */

export default Welcome
