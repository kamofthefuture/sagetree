import React, { Component, Fragment } from 'react'
import get from 'lodash/get'
import gql from 'graphql-tag'
import { Query } from 'react-apollo'
import { Text, Box, Image, Masonry, Sticky } from 'gestalt'
import Page from '../components/Page'
import Loader from '../components/Loader'
import RadarChart from '../components/RadarChart'
import InfoCard from '../components/InfoCard'
import { GENOME_TOKEN } from '../constant'

const oauthToken = localStorage.getItem(GENOME_TOKEN)
const GET_REPORT = gql`
  query getReport($scope: String!) {
    getReport(scope: $scope) {
      summary {
        score
        text
      }
    }
  }
`
const GET_VITAMIN = gql`
  query vitamin($id: ID!) {
    vitamin(where: { id: $id }) {
      id
      clientId
      prevents
      name
      createdAt
      updatedAt
      about
      helps
      sources
      dailyValue
      sideEffects
      products {
        id
        name
        createdAt
        updatedAt
        isOrganic
        price
        link
        img {
          src
          height
          width
        }
      }
      img {
        src
        height
        width
      }
      shouldTestFor
    }
  }
`
const GET_DISEASE = gql`
  query disease($id: ID!) {
    disease(where: { id: $id }) {
      id
      name
      about
      clientId
      takeVitamins
      shouldTestFor
      products {
        id
        name
        createdAt
        updatedAt
        isOrganic
        price
        link
        img {
          src
          height
          width
        }
      }
      img {
        src
        height
        width
      }
    }
  }
`

const getQuery = type => {
  switch (type) {
    case 'vitamin':
      return GET_VITAMIN
    case 'disease':
      return GET_DISEASE
    default:
      return null
  }
}

const Products = ({ products }) => (
  <Box marginBottom={6}>
    {products.map(p => (
      <Fragment>
        <Box marginBottom={2}>
          <Text bold size="md">
            Products
          </Text>
        </Box>
        <Box>
          <Text>{p.name}</Text>
          {p.img && (
            <Image
              alt={p.name}
              color="#393939"
              naturalHeight={p.img.height}
              naturalWidth={p.img.width}
              src={p.img.src}
            />
          )}
        </Box>
      </Fragment>
    ))}
  </Box>
)

const Content = ({ content, type }) => {
  let Component = null
  switch (type) {
    case 'vitamin':
      Component = (
        <Fragment>
          {oauthToken && (
            <Query
              query={GET_REPORT}
              variables={{
                scope: content.clientId,
              }}
            >
              {({ loading, error, data }) => {
                if (loading) return <Loader />
                return (
                  <Box marginBottom={6}>
                    <Box marginBottom={5}>
                      <Text bold size="md">
                        Genetics insight
                      </Text>
                    </Box>
                    <Text>
                      Your genetics results suggest you may have "
                      {get(data, 'getReport.summary.text', '')}" of{' '}
                      {content.name}. This result suggests you may need to{' '}
                      {get(data, 'getReport.summary.score', null) <= 2
                        ? 'increase'
                        : 'decrease'}{' '}
                      your {content.name} intake.
                    </Text>
                    <Text bold size="sm">
                      Please see products, meal plans, and food sources below.
                    </Text>
                  </Box>
                )
              }}
            </Query>
          )}
          <Box marginBottom={6}>
            <Box marginBottom={2}>
              <Text bold size="md">
                Helps
              </Text>
            </Box>
            <Text>{content.helps}</Text>
          </Box>
          <Box marginBottom={6}>
            <Box marginBottom={2}>
              <Text bold size="md">
                Recommended Daily Value
              </Text>
            </Box>
            <Text>{content.dailyValue}</Text>
          </Box>
          <Box marginBottom={6}>
            <Box marginBottom={2}>
              <Text bold size="md">
                Side Effects
              </Text>
            </Box>
            <Text>{content.sideEffects}</Text>
          </Box>
          <Box marginBottom={6}>
            <Box marginBottom={2}>
              <Text bold size="md">
                Where to Find {content.name}
              </Text>
            </Box>
            {content.sources.map(s => (
              <Text>{s}</Text>
            ))}
          </Box>
          {!!content.prevents.length && (
            <Box marginBottom={6}>
              <Box marginBottom={4}>
                <Text bold size="md">
                  Known to treat and prevent
                </Text>
              </Box>
              <Masonry
                minCols={1}
                virtualize
                columnWidth={270}
                items={content.prevents.map(id => ({ id }))}
                comp={({ data }) => (
                  <Query
                    query={GET_DISEASE}
                    variables={{
                      id: data.id,
                    }}
                  >
                    {({ loading, error, data }) => {
                      if (loading)
                        return (
                          <Box>
                            <Text>loading...</Text>
                          </Box>
                        )
                      return (
                        data && (
                          <InfoCard type="disease" content={data.disease} />
                        )
                      )
                    }}
                  </Query>
                )}
              />
            </Box>
          )}
        </Fragment>
      )
      break
    case 'disease':
      Component = (
        <Box marginBottom={6}>
          <Box marginBottom={5}>
            <Text bold size="md" marginBottom={3}>
              Genetics insights for {content.name}
            </Text>
            <Text size="md" marginTop={5} marginBottom={5}>
              coming soon
            </Text>
          </Box>

          <Box marginBottom={5}>
            <Text bold size="md" marginBottom={3}>
              Products to help treat {content.name}
            </Text>
            <Box
              marginTop={5}
              marginBottom={5}
              display="flex"
              justifyContent="center"
              alignItems="center"
              wrap
            >
              {Array(Math.floor(Math.random() * 6) + 2)
                .fill('')
                .map((_, i) => (
                  <Box marginRight={2}>
                    <img
                      key={i}
                      src="//www.placehold.it/175x175"
                      alt="placeholder"
                    />
                  </Box>
                ))}
            </Box>
            <Box marginBottom={5}>
              <Text bold size="md">
                Vitamin measure recommendation to decrease symptopms and aid
                against {content.name}
              </Text>
              <Box
                marginTop={5}
                marginBottom={5}
                display="flex"
                direction="column"
                justifyContent="center"
                alignItems="center"
              >
                <RadarChart />
              </Box>
            </Box>
          </Box>
          {!!content.takeVitamins.length && (
            <Box>
              <Box marginBottom={5}>
                <Text bold size="md">
                  Based on your profile we recommended these vitamins to prevent
                  and treat {content.name}
                </Text>
              </Box>
              <Masonry
                virtualize
                minCols={1}
                columnWidth={270}
                items={content.takeVitamins.map(id => ({ id }))}
                comp={({ data }) => (
                  <Box>
                    <Query
                      query={GET_VITAMIN}
                      variables={{
                        id: data.id,
                      }}
                    >
                      {({ loading, data }) => {
                        console.log(data)
                        if (loading)
                          return (
                            <Box>
                              <Text>loading...</Text>
                            </Box>
                          )
                        return (
                          data && (
                            <Box>
                              <InfoCard type="vitamin" content={data.vitamin} />
                            </Box>
                          )
                        )
                      }}
                    </Query>
                  </Box>
                )}
              />
            </Box>
          )}
        </Box>
      )
      break
    default:
      return null
  }

  return (
    <div>
      <Box marginBottom={5}>
        <Text bold size="xl">
          {content.name}
        </Text>
      </Box>

      <Box marginBottom={6}>
        <Text size="md">
          <div dangerouslySetInnerHTML={{ __html: content.about }} />
        </Text>
      </Box>
      {/* {content.products && <Products products={content.products} />} */}

      {Component}
    </div>
  )
}

export default class Details extends Component {
  static propTypes = {}

  render() {
    const type = this.props.match.params.type
    const id = this.props.match.params.id
    const query = getQuery(type)

    return (
      <Page header>
        {query && (
          <Query
            query={query}
            variables={{
              id,
            }}
          >
            {({ loading, data }) => {
              if (loading) return <Loader />
              const content = data[type]
              return (
                <Box
                  dangerouslySetInlineStyle={{
                    __style: {
                      maxWidth: '1100px',
                      margin: '0 auto'
                    },
                  }}
                >
                  <Content type={type} content={content} />
                  {/* <Sticky top={0} dangerouslySetZIndex={{ __zIndex: 3 }}>
                    <Box alignItems="center" color="lightGray" display="flex" height={40} position="relative" dangerouslySetInlineStyle={{ __style: { zIndex: 2 } }}>
                      <Text>This should also stick</Text>
                    </Box>
                  </Sticky> */}
                </Box>
              )
            }}
          </Query>
        )}
      </Page>
    )
  }
}
