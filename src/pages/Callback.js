import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

import Loader from '../components/Loader'

const SET_AUTH_SESSION = gql`
  mutation setAuthSession($url: String!) {
    GENOME_TOKEN: setAuthSession(url: $url)
  }
`

export default class Callback extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    console.log(nextProps, nextState)
  }


  render() {
    return (
      <Mutation
        mutation={SET_AUTH_SESSION}
      >
        {(setAuth, { loading, error, data }) => {
          if(loading) return <Loader />

          setAuth({
            variables: {
              url: window.location.href
            }
          }).then(data => {
            console.log(data)
            this.props.onAuth(data.data)
          })

          if (data) {
            //   props.onAuth(data.setAuthSession)
            return <Redirect to="/dashboard" />
          }
          
          return (
            null
          )
        }}
      </Mutation>
    )
  }
}


