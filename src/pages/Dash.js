import React from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import { Text, Masonry, Box, Image } from 'gestalt'
// import PropTypes from 'prop-types'

import Loader from '../components/Loader'
import InfoCard from '../components/InfoCard'
import Page from '../components/Page'

const GET_REPORTS = gql`
  query getReports($scope: [String!]!) {
    getReports(scope: $scope) {
      population
      phenotype {
        url_name
        display_name
        category
      }
      summary {
        score
        text
        warnings
      }
    }
  }
`
const GET_VITAMINS = gql`
  query vitamins {
    vitamins {
      id
      name
      clientId
      img {
        src
        width
        height
      }
    }
  }
`
const GET_DISEASES = gql`
  query diseases {
    diseases {
      id
      name
      clientId
      img {
        src
        width
        height
      }
    }
  }
`

const Dash = ({ oauthToken }) => {
  return (
    <Page>
      <Query query={GET_DISEASES}>
        {({ loading, error, data: { diseases } }) => {
          if (loading) return <Loader />

          return (
            <Box marginBottom={12}>
              <Box marginBottom={6}>
                <Text size="lg" bold>
                  Disease prevention and treatment
                </Text>
              </Box>
              <Masonry
                columnWidth={270}
                comp={content => (
                  <InfoCard content={content.data} type="disease" />
                )}
                items={diseases}
                minCols={1}
              />
            </Box>
          )
        }}
      </Query>

      <Query query={GET_VITAMINS}>
        {({ loading, error, data: { vitamins } }) => {
          if (loading) return <Loader />

          return (
            <Box marginBottom={12}>
              <Box marginBottom={6}>
                <Text size="lg" bold>
                  Vitamin catalog
                </Text>
              </Box>
              <Masonry
                columnWidth={270}
                comp={content => (
                  <InfoCard content={content.data} type="vitamin" />
                )}
                items={vitamins}
                minCols={1}
              />
            </Box>
          )
        }}
      </Query>

      {/* {
        oauthToken &&
        <Query
          query={GET_REPORTS}
          variables={{
            scope: [
              'vitamin-b12',
              'iron',
              'vitamin-d',
              'magnesium',
              'nicotine-dependence',
              'type-2-diabetes',
              'prostate-cancer',
              'breast-cancer',
              'facial-wrinkles',
              'weight',
              'vitamin-a',
              'calcium',
              'vitamin-e',
            ]
          }}
        >
          {({ loading, data }) => {
            const reports = get(data, 'getReports', [])
            if (loading) return <Loader />
            return (
              <Box marginBottom={12}>
                <Box marginBottom={6}>
                  <Text bold>Reports</Text>
                </Box>
                {reports.map(r => (
                  <Box>
                    <Text size="sm">{r.phenotype.display_name}</Text>
                    <Text>{r.summary.score}</Text>
                    <Text>{r.summary.text}</Text>
                  </Box>
                ))}
                <pre>
                  {JSON.stringify(reports, null, 3)}
                </pre>
              </Box>
            )
          }}
        </Query>
      } */}
    </Page>
  )
}

Dash.propTypes = {}

export default Dash
