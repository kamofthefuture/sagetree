import { keyframes } from 'styled-components'

export const scaleUp = keyframes`
	from {
		transform: scale(0);
	}
`

export const fadeIn = keyframes`
	from {
		opacity: 0;
	} to {
		opacity: 1;
	}
`
