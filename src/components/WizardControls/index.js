import React from 'react'
import { string, number, func, bool } from 'prop-types'
import styled from 'styled-components'
import { Button } from 'gestalt'
import { active, disabled } from './styles'

const ActionButton = styled(Button).attrs({
	inline: true
})`
	${active}
	${disabled}
`

const Controls = ({
	controlStyles, page, totalPages, onBack,
	onCancel, onNext, submitting, onSubmit,
	isForm, Cancel, Next
}) => {
	const isEnd = totalPages - 1 === page
	const C = page > 0 ? <ActionButton /> : Cancel
	const N = Next || ActionButton
	const renderNextButton = !isEnd && !isForm
	return (
		<div className={controlStyles}>

			{React.cloneElement(C, {
				variant: 'cancel',
				color: page > 0 ? 'grey' : 'red',
				text: page > 0 ? 'Previous' : 'Cancel',
				type: 'button',
				onClick: page > 0 ? onBack : onCancel,
				className: page > 0 ? 'previous' : 'cancel',
			})}

			{renderNextButton && React.cloneElement(N, {
				type: (isEnd && isForm) ? 'submit' : 'button',
				text: (page < (totalPages - 1)) ? 'Next' : 'Submit',
				disabled: submitting,
				color: 'blue',
				onClick: (isEnd && isForm) ? 'submit' : () => onNext()
			})}

		</div>
	)
}

Controls.propTypes = {
	controlStyles: string.isRequired,
	page: number.isRequired,
	totalPages: number.isRequired,
	onCancel: func.isRequired,
	onBack: func.isRequired,
	pristine: bool.isRequired,
	submitting: bool.isRequired,
}

export default Controls
