import { css } from 'styled-components'

export default css`
	&:disabled {
		opacity: 0.7;
		cursor: not-allowed;
	}
`
