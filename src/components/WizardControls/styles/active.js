import { css } from 'styled-components'

export default css`
	transition: opacity 350ms ease-in-out;
	&:active {
		transition: none;
		opacity: 0.4;
	}
`