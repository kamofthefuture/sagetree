import { compose } from 'recompact'

export default (...styledComponents) =>
	compose(...styledComponents.map(component => component.withComponent))