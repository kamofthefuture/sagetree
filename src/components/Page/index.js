import React from 'react'
import { keyframes } from 'styled-components'
import Cascade from '../Cascade'
import Nav from '../Nav'
import { withRouter } from 'react-router-dom'
import { Box, IconButton, Sticky } from 'gestalt'

const spin = keyframes`
   from {top: 0px;}
    to {top: 200px;}
`

class Page extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }

  render() {
    const { children, header, history, className } = this.props
    return (
      <div>
        {!header && <Nav />}
        <Box padding={6}>
          <Cascade className={className}>
            {
              header &&
              <Box marginBottom={6}>
                <IconButton
                  icon="arrow-back"
                  iconColor="gray"
                  accessibilityLabel="arrow-back"
                  onClick={() => history.push('/dashboard')}
                />
              </Box>
            }
            {children}
          </Cascade>
        </Box>
      </div>
    )
  }
}


export default withRouter(Page)