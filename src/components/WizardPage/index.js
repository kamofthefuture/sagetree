import React, { cloneElement } from 'react'
import { node, bool, number, func } from 'prop-types'
import styled from 'styled-components'
import Controls from '../WizardControls'

const Form = styled.form`
	display: flex;
	flex-direction: column;
	flex: 1;
	background-color: #eee;
	padding: 1rem;
	overflow: scroll;
`

const Wrapper = styled.div`
	flex: 1;
`

const focusFirstInput = (el) => {
	if (!el) return
	const firstInput = el.querySelector('input, select')
	if (firstInput) firstInput.focus()
}

const WizardPage = ({ className, children, handleSubmit, ...props }) => (
	<Form className={className} onSubmit={handleSubmit}>
		<Wrapper innerRef={focusFirstInput}>
			{cloneElement(children, { ...props })}
		</Wrapper>
		<Controls
			{...props}
		/>
	</Form>
)

WizardPage.propTypes = {
	children: node,
	submitting: bool.isRequired,
	pristine: bool.isRequired,
	page: number.isRequired,
	totalPages: number.isRequired,
	onNext: func.isRequired,
	onBack: func.isRequired,
	onCancel: func.isRequired,
	handleSubmit: func.isRequired,
}

export default WizardPage
