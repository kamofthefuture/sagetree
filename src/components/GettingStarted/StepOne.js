import React from 'react'
import { Box, Checkbox, Label, Text, } from 'gestalt'
import { compose, withState, withHandlers } from 'recompact'
import WizardPage from '../WizardPage'
import { REPORT_LIST } from '../../constant'

const enhance = compose(
  withState('checked', 'updateChecked', ({ checked }) => checked),
  withHandlers({
    onChange: ({
      list,
      id,
      label,
      updateList,
      updateChecked,
      checked
    }) => () => {
      updateList(() => {
        return (!checked) ? list.add(id) : (() => {
          list.delete(id)
          return list
        })()
      })

      updateChecked(!checked)
    },
  })
)

const CheckboxWithLabel = enhance(({ checked, onChange, id, label }) => (
  <Box alignItems="center" direction="row" display="flex" paddingY={2}>
    <Checkbox
      checked={checked}
      id={id}
      onChange={onChange}
    />
    <Label htmlFor={id}>
      <Box paddingX={2}>
        <Text size="sm">{label}</Text>
      </Box>
    </Label>
  </Box>
))

export default (props) =>
  <WizardPage {...props}>
    <div>
      <Box marginBottom={8}>
        <Text size="xl">Which health concepts would you like to be informed about?</Text>
      </Box>
      <Box wrap display="flex" direction="row" paddingY={2}>
        {
          Object.keys(REPORT_LIST)
            .map(k =>
              <div key={k}>
                <Text bold size="md">{k}</Text>
                {REPORT_LIST[k]
                  .map(t =>
                    <Box key={t}>
                      <CheckboxWithLabel
                        checked={props.list.has(t)}
                        list={props.list}
                        updateList={props.updateList}
                        id={t}
                        label={t.replace(/-/g, ' ')}
                      />
                    </Box>
                  )}
              </div>
            )
        }
      </Box>
    </div>
  </WizardPage>

