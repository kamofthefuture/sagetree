import React from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import { Box, Column, Text, Button } from 'gestalt'
import WizardPage from '../WizardPage'
import Loader from '../Loader'
import Link from '../Link'

const GET_AUTH_URL = gql`
  query getAuthUrl($scope: [String!]!) {
    url: getAuthUrl(scope: $scope) 
  }
`

export default (props) =>
  <WizardPage {...props}>
    <div>
      <Query
        query={GET_AUTH_URL}
        variables={{
          scope: Array.from(props.list)
        }}
      >
        {({ loading, error, data }) => {
          if (loading) return <Loader />
          return (
            <Box display="flex" direction="row" paddingY={2} wrap>
              <Column span={12} mdSpan={6}>
                <Box flex="grow" color="lightGray" padding={1}>
                  <Box color="white" paddingY={2}>
                    <Text align="center">Continue with genetics based recommendations</Text>
                  </Box>
                  <Box color="white" paddingY={2} align="center">
                    <a href={data.url}>
                      <Button
                        inline
                        text="Connect with genome"
                        color="blue"
                      />
                    </a>
                  </Box>
                </Box>
              </Column>
              <Column span={12} mdSpan={6}>
                <Box color="lightGray" padding={1}>
                  <Box color="white" paddingY={2}>
                    <Text align="center">Continue without genetic based recommendations</Text>
                  </Box>
                  <Box color="white" paddingY={2} align="center">
                    <Link to="/dashboard">
                      <Button
                        inline
                        text="Explore"
                        color="blue"
                        onClick={() => localStorage.setItem('selections', Array.from(props.list))}
                      />
                    </Link>
                  </Box>
                </Box>
              </Column>
            </Box>
          )
        }}
      </Query>
    </div>
  </WizardPage>
