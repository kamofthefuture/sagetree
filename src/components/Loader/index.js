import React from 'react'
import styled, { keyframes } from 'styled-components'
import { fadeIn } from '../../animations'

const rotate = keyframes`{
  to {
    transform: none;
  }
}`

const Wrapper = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const Dna = styled.div`
  width: 360px;
  margin: 100px auto;
  perspective: 200px;
  transform-style: preserve-3d;
  letter-spacing: -5px;
`

const Dot = styled.div`
  width: inherit;
  height: inherit;
  transform: translateZ(-20px);
  transform-style: preserve-3d;

  &:before {
    content: '';
    width: inherit;
    height: inherit;
    display: block;
    border-radius: 50%;
    animation: ${rotate} 3s linear infinite reverse;
    transform: rotateX(-360deg) translateZ(0);
  }

  &:nth-of-type(1n) {
    &:before {
        background-color: #aaa;
        height: 25px;
    }
  }
`

const Ele = styled.div`
  width: 6px;
  height: 6px;
  display: inline-block;
  animation: ${rotate} 3s linear infinite;
  transform: rotateX(-360deg) translateZ(0);
  transform-style: preserve-3d;
  margin-left: 0px;

  &:nth-of-type(1) &:before {
    animation-delay: -0.1s;
  }

  &:nth-of-type(1):nth-of-type(odd) {
    animation-delay: -1.6s;
  }

  &:nth-of-type(2),
  &:nth-of-type(2) &:before {
    animation-delay: -0.2s;
  }

  &:nth-of-type(2):nth-of-type(odd) {
    animation-delay: -1.7s;
  }

  &:nth-of-type(3),
  &:nth-of-type(3) &:before {
    animation-delay: -0.3s;
  }

  &:nth-of-type(3):nth-of-type(odd) {
    animation-delay: -1.8s;
  }

  &:nth-of-type(4),
  &:nth-of-type(4) &:before {
    animation-delay: -0.4s;
  }

  &:nth-of-type(4):nth-of-type(odd) {
    animation-delay: -1.9s;
  }

  &:nth-of-type(5),
  &:nth-of-type(5) &:before {
    animation-delay: -0.5s;
  }

  &:nth-of-type(5):nth-of-type(odd) {
    animation-delay: -2s;
  }

  &:nth-of-type(6),
  &:nth-of-type(6) &:before {
    animation-delay: -0.6s;
  }

  &:nth-of-type(6):nth-of-type(odd) {
    animation-delay: -2.1s;
  }

  &:nth-of-type(7),
  &:nth-of-type(7) &:before {
    animation-delay: -0.7s;
  }

  &:nth-of-type(7):nth-of-type(odd) {
    animation-delay: -2.2s;
  }

  &:nth-of-type(8),
  &:nth-of-type(8) &:before {
    animation-delay: -0.8s;
  }

  &:nth-of-type(8):nth-of-type(odd) {
    animation-delay: -2.3s;
  }

  &:nth-of-type(9),
  &:nth-of-type(9) &:before {
    animation-delay: -0.9s;
  }

  &:nth-of-type(9):nth-of-type(odd) {
    animation-delay: -2.4s;
  }

  &:nth-of-type(10),
  &:nth-of-type(10) &:before {
    animation-delay: -1s;
  }

  &:nth-of-type(10):nth-of-type(odd) {
    animation-delay: -2.5s;
  }

  &:nth-of-type(11),
  &:nth-of-type(11) &:before {
    animation-delay: -1.1s;
  }

  &:nth-of-type(11):nth-of-type(odd) {
    animation-delay: -2.6s;
  }

  &:nth-of-type(12),
  &:nth-of-type(12) &:before {
    animation-delay: -1.2s;
  }

  &:nth-of-type(12):nth-of-type(odd) {
    animation-delay: -2.7s;
  }

  &:nth-of-type(13),
  &:nth-of-type(13) &:before {
    animation-delay: -1.3s;
  }

  &:nth-of-type(13):nth-of-type(odd) {
    animation-delay: -2.8s;
  }

  &:nth-of-type(14),
  &:nth-of-type(14) &:before {
    animation-delay: -1.4s;
  }

  &:nth-of-type(14):nth-of-type(odd) {
    animation-delay: -2.9s;
  }

  &:nth-of-type(15),
  &:nth-of-type(15) &:before {
    animation-delay: -1.5s;
  }

  &:nth-of-type(15):nth-of-type(odd) {
    animation-delay: -3s;
  }

  &:nth-of-type(16),
  &:nth-of-type(16) &:before {
    animation-delay: -1.6s;
  }

  &:nth-of-type(16):nth-of-type(odd) {
    animation-delay: -3.1s;
  }

  &:nth-of-type(17),
  &:nth-of-type(17) &:before {
    animation-delay: -1.7s;
  }

  &:nth-of-type(17):nth-of-type(odd) {
    animation-delay: -3.2s;
  }

  &:nth-of-type(18),
  &:nth-of-type(18) &:before {
    animation-delay: -1.8s;
  }

  &:nth-of-type(18):nth-of-type(odd) {
    animation-delay: -3.3s;
  }

  &:nth-of-type(19),
  &:nth-of-type(19) &:before {
    animation-delay: -1.9s;
  }

  &:nth-of-type(19):nth-of-type(odd) {
    animation-delay: -3.4s;
  }

  &:nth-of-type(20),
  &:nth-of-type(20) &:before {
    animation-delay: -2s;
  }

  &:nth-of-type(20):nth-of-type(odd) {
    animation-delay: -3.5s;
  }

  &:nth-of-type(21),
  &:nth-of-type(21) &:before {
    animation-delay: -2.1s;
  }

  &:nth-of-type(21):nth-of-type(odd) {
    animation-delay: -3.6s;
  }

  &:nth-of-type(22),
  &:nth-of-type(22) &:before {
    animation-delay: -2.2s;
  }

  &:nth-of-type(22):nth-of-type(odd) {
    animation-delay: -3.7s;
  }

  &:nth-of-type(23),
  &:nth-of-type(23) &:before {
    animation-delay: -2.3s;
  }

  &:nth-of-type(23):nth-of-type(odd) {
    animation-delay: -3.8s;
  }

  &:nth-of-type(24),
  &:nth-of-type(24) &:before {
    animation-delay: -2.4s;
  }

  &:nth-of-type(24):nth-of-type(odd) {
    animation-delay: -3.9s;
  }

  &:nth-of-type(25),
  &:nth-of-type(25) &:before {
    animation-delay: -2.5s;
  }

  &:nth-of-type(25):nth-of-type(odd) {
    animation-delay: -4s;
  }

  &:nth-of-type(26),
  &:nth-of-type(26) &:before {
    animation-delay: -2.6s;
  }

  &:nth-of-type(26):nth-of-type(odd) {
    animation-delay: -4.1s;
  }

  &:nth-of-type(27),
  &:nth-of-type(27) &:before {
    animation-delay: -2.7s;
  }

  &:nth-of-type(27):nth-of-type(odd) {
    animation-delay: -4.2s;
  }

  &:nth-of-type(28),
  &:nth-of-type(28) &:before {
    animation-delay: -2.8s;
  }

  &:nth-of-type(28):nth-of-type(odd) {
    animation-delay: -4.3s;
  }

  &:nth-of-type(29),
  &:nth-of-type(29) &:before {
    animation-delay: -2.9s;
  }

  &:nth-of-type(29):nth-of-type(odd) {
    animation-delay: -4.4s;
  }

  &:nth-of-type(30),
  &:nth-of-type(30) &:before {
    animation-delay: -3s;
  }

  &:nth-of-type(30):nth-of-type(odd) {
    animation-delay: -4.5s;
  }

  &:nth-of-type(31),
  &:nth-of-type(31) &:before {
    animation-delay: -3.1s;
  }

  &:nth-of-type(31):nth-of-type(odd) {
    animation-delay: -4.6s;
  }

  &:nth-of-type(32),
  &:nth-of-type(32) &:before {
    animation-delay: -3.2s;
  }

  &:nth-of-type(32):nth-of-type(odd) {
    animation-delay: -4.7s;
  }

  &:nth-of-type(33),
  &:nth-of-type(33) &:before {
    animation-delay: -3.3s;
  }

  &:nth-of-type(33):nth-of-type(odd) {
    animation-delay: -4.8s;
  }

  &:nth-of-type(34),
  &:nth-of-type(34) &:before {
    animation-delay: -3.4s;
  }

  &:nth-of-type(34):nth-of-type(odd) {
    animation-delay: -4.9s;
  }

  &:nth-of-type(35),
  &:nth-of-type(35) &:before {
    animation-delay: -3.5s;
  }

  &:nth-of-type(35):nth-of-type(odd) {
    animation-delay: -5s;
  }

  &:nth-of-type(36),
  &:nth-of-type(36) &:before {
    animation-delay: -3.6s;
  }

  &:nth-of-type(36):nth-of-type(odd) {
    animation-delay: -5.1s;
  }

  &:nth-of-type(37),
  &:nth-of-type(37) &:before {
    animation-delay: -3.7s;
  }

  &:nth-of-type(37):nth-of-type(odd) {
    animation-delay: -5.2s;
  }

  &:nth-of-type(38),
  &:nth-of-type(38) &:before {
    animation-delay: -3.8s;
  }

  &:nth-of-type(38):nth-of-type(odd) {
    animation-delay: -5.3s;
  }

  &:nth-of-type(39),
  &:nth-of-type(39) &:before {
    animation-delay: -3.9s;
  }

  &:nth-of-type(39):nth-of-type(odd) {
    animation-delay: -5.4s;
  }

  &:nth-of-type(40),
  &:nth-of-type(40) &:before {
    animation-delay: -4s;
  }

  &:nth-of-type(40):nth-of-type(odd) {
    animation-delay: -5.5s;
  }

  &:nth-of-type(41),
  &:nth-of-type(41) &:before {
    animation-delay: -4.1s;
  }

  &:nth-of-type(41):nth-of-type(odd) {
    animation-delay: -5.6s;
  }

  &:nth-of-type(42),
  &:nth-of-type(42) &:before {
    animation-delay: -4.2s;
  }

  &:nth-of-type(42):nth-of-type(odd) {
    animation-delay: -5.7s;
  }

  &:nth-of-type(43),
  &:nth-of-type(43) &:before {
    animation-delay: -4.3s;
  }

  &:nth-of-type(43):nth-of-type(odd) {
    animation-delay: -5.8s;
  }

  &:nth-of-type(44),
  &:nth-of-type(44) &:before {
    animation-delay: -4.4s;
  }

  &:nth-of-type(44):nth-of-type(odd) {
    animation-delay: -5.9s;
  }

  &:nth-of-type(45),
  &:nth-of-type(45) &:before {
    animation-delay: -4.5s;
  }

  &:nth-of-type(45):nth-of-type(odd) {
    animation-delay: -6s;
  }

  &:nth-of-type(46),
  &:nth-of-type(46) &:before {
    animation-delay: -4.6s;
  }

  &:nth-of-type(46):nth-of-type(odd) {
    animation-delay: -6.1s;
  }

  &:nth-of-type(47),
  &:nth-of-type(47) &:before {
    animation-delay: -4.7s;
  }

  &:nth-of-type(47):nth-of-type(odd) {
    animation-delay: -6.2s;
  }

  &:nth-of-type(48),
  &:nth-of-type(48) &:before {
    animation-delay: -4.8s;
  }

  &:nth-of-type(48):nth-of-type(odd) {
    animation-delay: -6.3s;
  }

  &:nth-of-type(49),
  &:nth-of-type(49) &:before {
    animation-delay: -4.9s;
  }

  &:nth-of-type(49):nth-of-type(odd) {
    animation-delay: -6.4s;
  }

  &:nth-of-type(50),
  &:nth-of-type(50) &:before {
    animation-delay: -5s;
  }

  &:nth-of-type(50):nth-of-type(odd) {
    animation-delay: -6.5s;
  }

  &:nth-of-type(51),
  &:nth-of-type(51) &:before {
    animation-delay: -5.1s;
  }

  &:nth-of-type(51):nth-of-type(odd) {
    animation-delay: -6.6s;
  }

  &:nth-of-type(52),
  &:nth-of-type(52) &:before {
    animation-delay: -5.2s;
  }

  &:nth-of-type(52):nth-of-type(odd) {
    animation-delay: -6.7s;
  }

  &:nth-of-type(53),
  &:nth-of-type(53) &:before {
    animation-delay: -5.3s;
  }

  &:nth-of-type(53):nth-of-type(odd) {
    animation-delay: -6.8s;
  }

  &:nth-of-type(54),
  &:nth-of-type(54) &:before {
    animation-delay: -5.4s;
  }

  &:nth-of-type(54):nth-of-type(odd) {
    animation-delay: -6.9s;
  }

  &:nth-of-type(55),
  &:nth-of-type(55) &:before {
    animation-delay: -5.5s;
  }

  &:nth-of-type(55):nth-of-type(odd) {
    animation-delay: -7s;
  }

  &:nth-of-type(56),
  &:nth-of-type(56) &:before {
    animation-delay: -5.6s;
  }

  &:nth-of-type(56):nth-of-type(odd) {
    animation-delay: -7.1s;
  }

  &:nth-of-type(57),
  &:nth-of-type(57) &:before {
    animation-delay: -5.7s;
  }

  &:nth-of-type(57):nth-of-type(odd) {
    animation-delay: -7.2s;
  }

  &:nth-of-type(58),
  &:nth-of-type(58) &:before {
    animation-delay: -5.8s;
  }

  &:nth-of-type(58):nth-of-type(odd) {
    animation-delay: -7.3s;
  }

  &:nth-of-type(59),
  &:nth-of-type(59) &:before {
    animation-delay: -5.9s;
  }

  &:nth-of-type(59):nth-of-type(odd) {
    animation-delay: -7.4s;
  }

  &:nth-of-type(60),
  &:nth-of-type(60) &:before {
    animation-delay: -6s;
  }

  &:nth-of-type(60):nth-of-type(odd) {
    animation-delay: -7.5s;
  }
`

const FadingText = styled.p`
  animation: ${fadeIn} 800ms ease-in-out infinite;
  animation-fill-mode: both;
  animation-direction: alternate;
  letter-spacing: 5px !important;
  display: block;
  padding-top: 50px;
  color: #aaa;
  text-align: center;
  text-transform: uppercase;
`
const Loader = ({ className, withText }) => {
  return (
    <Wrapper>
      <Dna className={className}>
        {Array(60)
          .fill('')
          .map((t, i) =>
            <Ele key={i}>
              <Dot />
            </Ele>
          )
        }
      </Dna>
      {withText && <FadingText color="olive">Loading...</FadingText>}
    </Wrapper>
  )
}

Loader.defaultProps = {
  withText: true
}

export default Loader