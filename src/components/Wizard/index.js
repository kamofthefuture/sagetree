import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { compose, withState, withHandlers, withProps } from 'recompact'
import Cascade from '../Cascade'
import { scaleUp } from '../../animations'


const enhance = compose(
	withProps(({ pages }) => ({
		totalPages: pages.length,
		pageMap: pages.map(({ step }) => ({
			step,
		})),
	})),
	withState('page', 'updatePage', 0),
	withHandlers({
		nextPage: ({ updatePage, totalPages }) => () => updatePage((n) => Math.min(n + 1, totalPages - 1)),
		prevPage: ({ updatePage }) => () => updatePage((n) => Math.max(n - 1, 0)),
	})
)

const Container = styled(Cascade).attrs({ animation: scaleUp })`
	position: absolute;
	top: 2rem;
	left: 2rem;
	right: 2rem;
	bottom: 2rem;
	height: 90vh;
	display: flex;
	flex-direction: column;
`

const Wizard = ({
	page, pages, totalPages,
	onSubmit, nextPage, prevPage,
	pageMap, onCancel, forceUnregisterOnUnmount,
	...props
}) => {
	const Component = pages[page].component

	const Page = Component

	return (
		<Container>
			<Page
				onSubmit={(page === (totalPages - 1)) ? onSubmit : nextPage}
				onNext={nextPage}
				onBack={prevPage}
				onCancel={onCancel}
				page={page}
				totalPages={totalPages}
				{...props}
			/>
		</Container>
	)
}

Wizard.propTypes = {
	pages: PropTypes.arrayOf(PropTypes.shape({ name: PropTypes.string, step: PropTypes.string, component: PropTypes.node })).isRequired,
	pageMap: PropTypes.arrayOf(PropTypes.shape({ step: PropTypes.string })).isRequired,
	page: PropTypes.number.isRequired,
	totalPages: PropTypes.number.isRequired,
	nextPage: PropTypes.func.isRequired,
	prevPage: PropTypes.func.isRequired,
	onSubmit: PropTypes.func.isRequired,
}

export default enhance(Wizard)
