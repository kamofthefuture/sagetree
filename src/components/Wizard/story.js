import React from 'react'
import { storiesOf } from '@storybook/react'
import { withProps, compose } from 'recompact'
import { createValuesComponent, formDecorator } from '../storyUtils'
import Wizard from '.'
import WizardPage from '../WizardPage'
import { TextField, SelectField, RadioField, WizardProgress, Bubbles } from '../'

const FORM_NAME = 'Wizard'
const Values = createValuesComponent(FORM_NAME)

const radioValues = [
	{ value: 'Option A' },
	{ value: 'Option B' },
	{ value: 'Option C' },
	{ value: 'Option D' },
]

const pageMap = [
	{ step: 'details' },
	{ step: 'tree' },
	{ step: 'tests'	},
	{ step: 'questions' 	},
]

const required = (value) => {
	if (!value) {
		return 'Required!'
	}
}

const StepOne = (props) =>
	<WizardPage {...props}>
		<TextField label="Name" name="name" validate={required} />
		<TextField label="Age" name="age" validate={required} />
		<TextField label="Height" name="height" />
		<TextField label="Weight" name="weight" />
		<TextField label="Favorite language" name="language" placeholder="Javascript" />
	</WizardPage>

const StepTwo = (props) =>
	<WizardPage {...props}>
		<SelectField
			name="mart"
			label="Select a mart"
			options={[{ value: 'Jet' }, { value: 'Walmart' }]}
		/>
	</WizardPage>

const StepThree = compose(
	withProps({
		children: <TextField variant="big" label="Scream!" name="scream" />,
	}))(WizardPage)

const StepFour = compose(
	withProps({
		children:
	<div>
		<TextField variant="big" label="good?" name="youGood" />
		<RadioField values={radioValues} name="radios" />
	</div>,
	}))(WizardPage)


const pages = [
	{ name: 'StepOne', step: 'who', component: StepOne },
	{ name: 'StepTwo', step: 'mart', component: StepTwo },
	{ name: 'StepThree', step: 'loud', component: StepThree	},
	{ name: 'StepFour', step: 'sentiment', component: StepFour	},
]

storiesOf('Wizard', module)
	.add('Form', () =>
		<div style={{ height: '100%' }}>
			<Wizard
				form={FORM_NAME}
				onSubmit={(props) => alert(JSON.stringify(props, null, 2))}
				onCancel={() => alert('Cancel!')}
				pages={pages}
			/>
			<Values />
		</div>
	)
	.add('Progress', () =>
		<WizardProgress form={FORM_NAME} pageMap={pages} active={0} />
	).add('Bubbles', () => {
		const activeBubble = Math.floor(Math.random() * 4)
		return (
			<div>
				<Bubbles pageMap={pageMap} active={activeBubble} />
				<h1>Bubble #{activeBubble + 1} is active!</h1>
				<hr />
				<h3>These are your form "steps"</h3>
				<pre>{JSON.stringify(pageMap, null, 3)}</pre>
				<hr />

			</div>
		)
	}
	)
	.addDecorator(formDecorator)
	.add('Page', () => <StepOne />)
