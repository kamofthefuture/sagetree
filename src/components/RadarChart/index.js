import React from 'react'
import {
  VictoryArea,
  VictoryGroup,
  VictoryPolarAxis,
  VictoryLabel,
  VictoryChart,
  VictoryTheme,
} from 'victory'

const characterData = [
  { vitaminB12: 1, iron: 250, magnesium: 1, vitaminD: 40 },
  { vitaminB12: 2, iron: 300, magnesium: 2, vitaminD: 80 },
  { vitaminB12: 5, iron: 225, magnesium: 3, vitaminD: 60 },
]

class RadarChart extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: this.processData(characterData),
      maxima: this.getMaxima(characterData),
    }
  }

  getMaxima(data) {
    const groupedData = Object.keys(data[0]).reduce((memo, key) => {
      memo[key] = data.map(d => d[key])
      return memo
    }, {})
    return Object.keys(groupedData).reduce((memo, key) => {
      memo[key] = Math.max(...groupedData[key])
      return memo
    }, {})
  }

  processData(data) {
    const maxByGroup = this.getMaxima(data)
    const makeDataArray = d => {
      return Object.keys(d).map(key => {
        return { x: key, y: d[key] / maxByGroup[key] }
      })
    }
    return data.map(datum => makeDataArray(datum))
  }

  render() {
    return (
      <div
        style={{
          maxWidth: '300px'
        }}
      >
        <VictoryChart
          width={400}
          polar
          theme={VictoryTheme.material}
          domain={{ y: [0, 1] }}
        >
          <VictoryGroup
            colorScale={['gold', 'orange', 'tomato']}
            style={{ data: { fillOpacity: 0.2, strokeWidth: 2 } }}
          >
            {this.state.data.map((data, i) => {
              return <VictoryArea key={i} data={data} />
            })}
          </VictoryGroup>
          {Object.keys(this.state.maxima).map((key, i) => {
            return (
              <VictoryPolarAxis
                width={400}
                key={i}
                dependentAxis
                style={{
                  axisLabel: { padding: 10 },
                  axis: { stroke: 'none' },
                  grid: { stroke: 'grey', strokeWidth: 0.25, opacity: 0.5 },
                }}
                tickLabelComponent={<VictoryLabel labelPlacement="vertical" />}
                labelPlacement="perpendicular"
                axisValue={i + 1}
                label={key}
                tickFormat={t => Math.ceil(t * this.state.maxima[key])}
                tickValues={[0.25, 0.5, 0.75]}
              />
            )
          })}
          <VictoryPolarAxis
            width={400}
            labelPlacement="parallel"
            tickFormat={() => ''}
            style={{
              axis: { stroke: 'none' },
              grid: { stroke: 'grey', opacity: 0.5 },
            }}
          />
        </VictoryChart>
      </div>
    )
  }
}

export default RadarChart
