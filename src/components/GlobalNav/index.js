import React from 'react'
// import { NavLink, Link } from 'react-router-dom'
// import { AUTH_TOKEN } from '../../constant'

export default ({ data, token }) => {
  return (
    <nav></nav>
  )
}

/**
 * 
 *  <Link className="link dim black b f6 f5-ns dib mr3" to="/" title="Feed">
        Blog
        </Link>
      <NavLink
        className="link dim f6 f5-ns dib mr3 black"
        activeClassName="gray"
        exact={true}
        to="/"
        title="Feed"
      >
        Feed
        </NavLink>
      {data &&
        data.me &&
        data.me.email &&
        token && (
          <NavLink
            className="link dim f6 f5-ns dib mr3 black"
            activeClassName="gray"
            exact={true}
            to="/drafts"
            title="Drafts"
          >
            Drafts
            </NavLink>
        )}
      {token ? (
        <div
          onClick={() => {
            this.refreshTokenFn &&
              this.refreshTokenFn({
                [AUTH_TOKEN]: null,
              })
            window.location.href = '/'
          }}
          className="f6 link dim br1 ba ph3 pv2 fr mb2 dib black"
        >
          Logout
          </div>
      ) : (
          <Link
            to="/login"
            className="f6 link dim br1 ba ph3 pv2 fr mb2 dib black"
          >
            Login
          </Link>
        )}
      {data &&
        data.me &&
        data.me.email &&
        token && (
          <Link
            to="/create"
            className="f6 link dim br1 ba ph3 pv2 fr mb2 dib black"
          >
            + Create Draft
            </Link>
        )}
 * 
 */