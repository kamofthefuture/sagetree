import React from 'react'
import { withRouter } from 'react-router-dom'
import { withState, compose } from 'recompact'
import { Box, Text, Sticky, IconButton, Toast } from 'gestalt'
import Link from '../Link/index'
import { GENOME_TOKEN } from '../../constant'
import Cascade from '../Cascade'

const enhance = compose(
  withState('showToast', 'onLogoutToast', false),
  withState('isLoggedOut', 'setLoggedOut', false),
  withRouter,
)

export default enhance(
  ({
    header,
    showToast,
    onLogoutToast,
    setLoggedOut,
    isLoggedOut,
    history,
  }) => {
    const oauthToken = localStorage.getItem(GENOME_TOKEN)
    const logout = () => {
      localStorage.removeItem(GENOME_TOKEN)
      onLogoutToast(true)
      setTimeout(() => {
        onLogoutToast(false)
        setLoggedOut(true && oauthToken)
      }, 1500)
    }
    return (
      <div>
        <Sticky top={0}>
          <Box
            alignItems="center"
            direction="row"
            display="flex"
            color="olive"
            marginBottom={2}
            padding={4}
          >
            <Box flex="grow">
              <Link to="/dashboard">
                <Text color="white" size="xl" bold>
                  Sagetree
                </Text>
              </Link>
            </Box>
            {oauthToken && !isLoggedOut ? null : (
              <Box display="flex" alignItems="center">
                <Text color="white" size="xs">
                  wellness through genetic insight
                </Text>
                <IconButton
                  size="sm"
                  icon="lightbulb"
                  iconColor="lightGray"
                  onClick={() => history.push('/entry')}
                />
              </Box>
            )}
          </Box>

          <Box
            fit
            dangerouslySetInlineStyle={{
              __style: {
                bottom: 250,
                left: '50%',
                transform: 'translateX(-50%)',
              },
            }}
            paddingX={1}
            position="fixed"
          >
            {showToast ? (
              <Cascade>
                <Toast color="orange" text="Logged out" />
              </Cascade>
            ) : null}
          </Box>
        </Sticky>
      </div>
    )
  },
)
