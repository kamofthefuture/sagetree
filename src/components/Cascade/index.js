import { Children } from 'react'
import styled from 'styled-components'
import { defaultProps } from 'recompact'
import { fadeIn } from '../../animations'

export default defaultProps({
  animation: fadeIn
})(styled.div`
  > * {
    animation: ${({ animation }) => animation} 500ms ease-out backwards;
    ${({ children, interval = 50 }) =>
    Children.map(
      children,
      (child, i) => `
      &:nth-child(${i + 1}n) {
        animation-delay: ${i * interval}ms;
      }
    `
    )};
  }
`)