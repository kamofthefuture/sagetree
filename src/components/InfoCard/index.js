import React from 'react'
import { Text, Box, Image } from 'gestalt'
import Link from '../Link'

const getRandomFromArray = arr => arr[Math.floor(Math.random() * arr.length)]

export default ({ content, type }) => {
  
  const color = getRandomFromArray([
    '#2b3938',
    '#8e7439',
    '#698157',
    '#4e5d50',
    '#6d6368',
  ])

  return (
    <Box>
      <Link to={`/detail/${type}/${content.id}`}>
        <Image
          alt={content.name}
          color={color}
          naturalHeight={content.img.height}
          naturalWidth={content.img.width}
          src={content.img.src}
        >
          <Box padding={3}>
            <Text color="white" bold size="md">
              {content.name}
            </Text>
          </Box>
        </Image>
      </Link>
    </Box>
  )
}
