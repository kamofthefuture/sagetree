import React, { Component, Fragment } from 'react'
import { Router, Route, Switch } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import { AUTH_TOKEN, GENOME_TOKEN } from '../constant'
import { isTokenExpired } from '../helper/jwtHelper'
import NotFound from '../pages/NotFound'
import Welcome from '../pages/Welcome'
import Dash from '../pages/Dash'
import Entry from '../pages/Entry'
import Details from '../pages/Details'
import Callback from '../pages/Callback'
import Logout from '../pages/Logout'

const history = createBrowserHistory()

class RootContainer extends Component {
  constructor(props) {
    super(props)
    this.refreshTokenFn = this.refreshTokenFn.bind(this)
    this.oauthTokenFn = this.oauthTokenFn.bind(this)

    this.state = {
      token: props.token,
      oauthToken: props.oauthToken
    }
  }

  refreshTokenFn(data = {}) {
    const token = data.AUTH_TOKEN

    if (token) {
      localStorage.setItem(AUTH_TOKEN, token)
    } else {
      localStorage.removeItem(AUTH_TOKEN)
    }

    this.setState({
      token: data.AUTH_TOKEN,
    })
  }

  oauthTokenFn(data = {}) {
    localStorage.removeItem(GENOME_TOKEN)

    const oauthToken = data[GENOME_TOKEN]

    if (oauthToken) {
      localStorage.setItem(GENOME_TOKEN, oauthToken)
    } else {
      localStorage.removeItem(GENOME_TOKEN)
    }

    this.setState({
      oauthToken
    })
  }

  bootStrapData() {
    try {
      // const token = localStorage.getItem(AUTH_TOKEN)
      const oauthToken = localStorage.getItem(GENOME_TOKEN)

      // if (token !== null && token !== undefined) {
      //   const expired = isTokenExpired(token)
      //   if (!expired) {
      //     this.setState({ token: token })
      //   } else {
      //     localStorage.removeItem(AUTH_TOKEN)
      //     this.setState({ token: null })
      //   }
      // }

      if (oauthToken !== null && oauthToken !== undefined) {
        const expired = isTokenExpired(oauthToken)
        if (!expired) {
          this.setState({ oauthToken: oauthToken })
        } else {
          localStorage.removeItem(GENOME_TOKEN)
          this.setState({ oauthToken: null })
        }
      }

    } catch (e) {
      console.log('')
    }
  }

  //verify localStorage check
  componentDidMount() {
    this.bootStrapData()
  }

  render() {
    return (
      <Router onUpdate={() => window.scrollTo(0, 0)} history={history}>
        <Fragment>
          <Switch>
            <Route exact path="/" component={Welcome} />
            <Route exact path="/entry" component={Entry} />
            <Route exact path="/dashboard" component={() => <Dash oauthToken={this.state.oauthToken} /> } />
            <Route exact path="/detail/:type/:id" component={Details} />
            <Route exact path="/callback" component={() => <Callback onAuth={this.oauthTokenFn} />} />
            <Route exact path="/logout" component={Logout} />
            <Route component={NotFound} />
          </Switch>
        </Fragment>
      </Router>
    )
  }
}

// const ME_QUERY = gql`
//   query MeQuery {
//     me {
//       id
//       email
//       name
//     }
//   }
// `

export default RootContainer


/**
 *
 * <ProtectedRoute
            token={this.state.token}
            path="/drafts"
            component={DraftsPage}
          />
          <ProtectedRoute
            token={this.state.token}
            path="/create"
            component={CreatePage}
          />
          <Route path="/post/:id" component={DetailPage} />
          <Route
            token={this.state.token}
            path="/login"
            render={props => <LoginPage refreshTokenFn={this.refreshTokenFn} />}
          />
          <Route
            token={this.state.token}
            path="/signup"
            render={props => (
              <SignupPage refreshTokenFn={this.refreshTokenFn} />
            )}
          />
          <Route path="/logout" component={LogoutPage} />
 */